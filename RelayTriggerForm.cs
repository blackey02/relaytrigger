﻿using RelayArrayConfigurationNs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RelayTrigger
{
    public partial class RelayTriggerForm : Form
    {
        private RelayArrayConfiguration _relayArrayConfiguration = new RelayArrayConfiguration();

        static RelayTriggerForm()
        {
            System.Net.ServicePointManager.Expect100Continue = false;
        }

        private void SetRelayArrayUrl()
        {

            string socketRegex = "localhost[:0-9]*";
            string socketRegex2 = @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b[:0-9]*";
            string socket = string.Format("{0}:{1}", Properties.Settings.Default.CameraIp, Properties.Settings.Default.CameraPort.ToString());
            if(Regex.IsMatch(_relayArrayConfiguration.Url, socketRegex))
                _relayArrayConfiguration.Url = Regex.Replace(_relayArrayConfiguration.Url, socketRegex, socket);
            else if (Regex.IsMatch(_relayArrayConfiguration.Url, socketRegex2))
                _relayArrayConfiguration.Url = Regex.Replace(_relayArrayConfiguration.Url, socketRegex2, socket);
        }

        public RelayTriggerForm()
        {
            InitializeComponent();

            textBoxIp.Text = Properties.Settings.Default.CameraIp;
            textBoxPort.Text = Properties.Settings.Default.CameraPort.ToString();
            textBoxPolarity.Text = Properties.Settings.Default.RelayPolarity.ToString();
            textBoxMode.Text = Properties.Settings.Default.RelayMode.ToString();
            textBoxPeriod.Text = Properties.Settings.Default.RelayPeriodMs.ToString();
            textBoxDutyCycle.Text = Properties.Settings.Default.RelayDutyCycle.ToString();
            textBoxPulses.Text = Properties.Settings.Default.RelayPulses.ToString();
            SetRelayArrayUrl();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default.CameraIp = textBoxIp.Text;
                Properties.Settings.Default.CameraPort = ushort.Parse(textBoxPort.Text);
                Properties.Settings.Default.RelayPolarity = ushort.Parse(textBoxPolarity.Text);
                Properties.Settings.Default.RelayMode = ushort.Parse(textBoxMode.Text);
                Properties.Settings.Default.RelayPeriodMs = ushort.Parse(textBoxPeriod.Text);
                Properties.Settings.Default.RelayDutyCycle = ushort.Parse(textBoxDutyCycle.Text);
                Properties.Settings.Default.RelayPulses = ushort.Parse(textBoxPulses.Text);
                Properties.Settings.Default.Save();
                SetRelayArrayUrl();
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error: {0}", ex.Message), "Alert");
            }
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            textBoxIp.Text = "192.168.1.100";
            textBoxPort.Text = "80";
            textBoxPolarity.Text = "0";
            textBoxMode.Text = "3";
            textBoxPeriod.Text = "500";
            textBoxDutyCycle.Text = "50";
            textBoxPulses.Text = "1";
            buttonSave_Click(this, EventArgs.Empty);
        }

        private void buttonTrigger_Click(object sender, EventArgs e)
        {
            try
            {
                var relayConfig = new RelayConfig();
                relayConfig.polarity = Properties.Settings.Default.RelayPolarity;
                relayConfig.enabled = 0;
                relayConfig.mode = Properties.Settings.Default.RelayMode;
                relayConfig.period = Properties.Settings.Default.RelayPeriodMs / 100;
                relayConfig.dutyCycle = Properties.Settings.Default.RelayDutyCycle;
                relayConfig.pulses = Properties.Settings.Default.RelayPulses;
                relayConfig.physicalOutput = 1;
                _relayArrayConfiguration.TriggerRelay(0, Properties.Settings.Default.RelayMode, relayConfig);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error: {0}", ex.Message), "Alert");
            }
        }
    }
}
